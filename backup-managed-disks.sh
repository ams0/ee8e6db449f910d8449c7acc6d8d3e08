#!/bin/bash -e

backuprg=prd-northeurope-backup-rg
location=northeurope

for name in `az resource list --tag backup=true --query "[?type=='Microsoft.Compute/disks']".name -o tsv`; do
for id in `az resource list --tag backup=true --query "[?type=='Microsoft.Compute/disks' && name=='$name'].id" -o tsv`; do
  rg=`az resource list --tag backup=true --query "[?type=='Microsoft.Compute/disks' && name=='$name'].resourceGroup" -o tsv`;
  az snapshot create --tags createdby=backupscript --incremental -l $location -g $backuprg --source $id --name $name-snap-$rg-`date '+%Y-%m-%d'` > /dev/null 2>&1; 
  echo "Incremental snapshot created from disk $name, saved as $name-snap-$rg-`date '+%Y-%m-%d'` in resource group $backuprg, tagged with createdby = backupscript"; 
done; 
done

#Delete snapshot older than a month

for snapshot in `az snapshot list -g $backuprg --query "[?tags.createdby == 'backupscript' && timeCreated < '$(date -d "-1 month" --iso-8601=seconds)'].[name]" -o tsv`; do
az snapshot delete -g $backuprg --name $snapshot;
echo "Snapshot $snapshot in $backuprg resource group deleted" ;
done
